# frozen_string_literal: true

RSpec.describe Strum::Json do
  it "has a version number" do
    expect(Strum::Json::VERSION).not_to be nil
  end
end
