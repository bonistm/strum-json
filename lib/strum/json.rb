# frozen_string_literal: true

require_relative "json/version"
require_relative "json/serializer"
require_relative "json/deserializer"
require_relative "json/deep_keys_to_sym"
require_relative "json/schema"

module Strum
  module Json
    class Error < StandardError; end
    # Your code goes here...
  end
end
