# frozen_string_literal: true

require "strum/json/serializer_class"
require "dry/inflector"

module Strum
  module Json
    module Serializer
      def self.const_missing(name)
        Class.new do
          include Strum::Service
          define_method :call do
            require "serializers/#{Dry::Inflector.new.underscore(name)}_serializer"
            serializer_class = Strum::Json::SerializerClass.call(name: name)
            output(serializer_class.new(model, options).serializable_hash)
          end

          define_method :model do
            return input if input.is_a?(Sequel::Model)
            return input if input.is_a?(Dry::Struct)
            return input if input.is_a?(Array) && (input.first.is_a?(Sequel::Model) || input.first.is_a?(Dry::Struct))

            input[:model]
          end

          define_method :options do
            return {} if input.is_a?(Sequel::Model)
            return {} if input.is_a?(Dry::Struct)
            return {} if input.is_a?(Array)

            input.except(:model)
          end
        end
      end

      def self.call(model, **options)
        const_get(model.class.name.split("::").last, false).call(options.merge(model: model))
      end
    end
  end
end
