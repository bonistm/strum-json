# frozen_string_literal: true

require "dry-inflector"
require "strum/service"

module Strum
  module Json
    # Deserialize JSON-API to hash(s)
    class Deserializer
      include Strum::Service

      def call
        output(root_data[:data] ? prepare_output : root_errors[:errors])
      end

      def audit
        add_error(:root, :data_and_errors_must_not_coexist) unless !root_data[:data] ^ !root_errors[:errors]
      end

      def root_data
        @root_data ||= deep_transform_keys(input)
      end

      def root_errors
        @root_errors ||= deep_transform_keys(input)
      end

      def deep_transform_keys(object)
        keys_to_sym = lambda do |object|
          case object
          when Hash
            object.keys.each do |key|
              value = object.delete(key)
              object[(key.respond_to?(:to_sym) ? inflector.underscore(key).to_sym : key)] = keys_to_sym[value]
            end
            object
          when Array
            object.map! { |e| keys_to_sym[e] }
          else
            object
          end
        end
        merge_relationships = lambda do |object|
          handle_attributes(object)
        end
        result = keys_to_sym >> merge_relationships
        result[object]
      end

      def merge_relations(hash)
        if hash[:data].is_a?(Array)
          parse_array_relationships(hash)
        else
          relationships = relations(hash)
          included = includes(hash)
          prepare_relationships = lambda { |hash|
            hash.each do |key, value|
              if value.is_a?(Hash)
                hash[key.to_sym] = (value.values_at(:attributes)&.first || value.values_at(:data)&.first || value)
              end
            end
          }
          if relationships && hash[:data][:attributes]
            hash[:data][:attributes].merge!(prepare_relationships[relationships])
          end
          hash[:data][:attributes].merge!(prepare_includes(included)) if included && hash[:data][:attributes]
        end
        hash
      end

      def parse_array_relationships(hash)
        prepare_nested = lambda { |hash|
          hash.each do |key, value|
            hash[key.to_sym] = value.values_at(:data)&.first if value.is_a?(Hash)
          end
        }
        hash[:data].each do |element|
          relationships, included = element.values_at(:relationships, :included)
          element[:attributes].merge!(prepare_nested[relationships]) if relationships
          element[:attributes].merge!(prepare_nested[included]) if included
        end
      end

      def relations(hash)
        case hash[:data]
        when Array
          hash[:data].map { |i| i[:relationships] }
        when Hash
          hash[:data][:relationships]
        else
          hash[:data]
        end
      end

      def includes(hash)
        hash[:included]
      end

      def prepare_includes(includes)
        includes.each_with_object({}) do |h, e|
          e[h[:type].to_sym] ||= []
          e[h[:type].to_sym] << h[:attributes]
        end
      end

      def handle_attributes(object)
        case object
        when Array
          object.map { |i| merge_relations(i) }
        when Hash
          merge_relations(object)
        else
          object
        end
      end

      def prepare_output
        case root_data
        when Array
          root_data.map { |i| i[:data][:attributes] }
        when Hash
          root_data[:data].is_a?(Array) ? root_data[:data].map { |i| i[:attributes] } : root_data[:data][:attributes]
        else
          root_data
        end
      end

      def inflector
        @inflector = Dry::Inflector.new
      end
    end
  end
end
