# frozen_string_literal: true

require "strum/service"
require "dry-inflector"

module Strum
  module Json
    class DeepKeysToSym
      include Strum::Service

      def call
        output(deep_transform_keys(input))
      end

      def audit
        required
      end

      private

        def deep_transform_keys(hash)
          return hash unless hash.is_a?(Hash)

          hash.each_with_object({}) do |(key, value), result|
            result[inflector.underscore(key).to_sym] = deep_transform_keys(value)
          end
        end

        def inflector
          @inflector ||= Dry::Inflector.new
        end
    end
  end
end
